import { SELECT_ITEM, SHOW_DETAILS, ITEMS_FETCH_DATA_SUCCESS } from '../constants/ActionTypes'

const initialState = {
  items: [{
    selected: 0,
    display: false,
    name: "",
    tagline: "",
    description: "",
    image_url: ""
  }],
  hasError: false,
  isLoading: false
}

export default function beers(state = initialState, action) {
  
  switch (action.type) {
    
    case SELECT_ITEM:
      return state.filter(beer =>
        beer.id === action.id
      )

    case SHOW_DETAILS:
      return state.filter(beer =>
        beer.id === action.id
      )

    case ITEMS_FETCH_DATA_SUCCESS:
      return action.items

    default:
      return state
  }
}