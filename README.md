# Fractal project

## Features

* React 16
* Webpack 3
* Babel
* Staging ES Next Features
* Hot Module Replacement

## Installation

* `git clone `
* npm install
* npm start
* visit `http://localhost:8080/`
