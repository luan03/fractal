import React, { Component } from 'react'
import { connect } from 'react-redux'
import Card from '../components/Card'


class List extends Component {
  
  constructor(props) {
      super(props)

      this.state = {
          items: [],
          selected: {}
      };
  }

  fetchData(url) {

      fetch(url)
          .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                return response
          })
          .then((response) => response.json())
          .then((items) => {
            
            const beers = items.map(beer => {    
                let {name, tagline, description, image_url, ...rest} = beer;
                beer = {name, tagline, description, image_url}

                return beer;
            })
            
            items = beers;
    
            this.setState({ items })

          })
    }

    componentDidMount() {
      this.fetchData('https://api.punkapi.com/v2/beers');
    }
      
    updateCard = (event) => {
        this.setState({ selected: this.state.items[event.currentTarget.value] })
    };

  render() {
        const { items } = this.state
        
        return (!items.length) ? <div>No country Names</div> :
            <div>
                <select className="items-list" onChange={this.updateCard}>
                    {items.map(
                        (x,i) => <option key={i} value={i}>{x.name} - {x.tagline}</option>
                    )}
                </select>

                <h2>Beer Information</h2>

                <Card selected={this.state.selected} />

            </div>
    }  
} 

export default connect()(List);

module.hot.accept();