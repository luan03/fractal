import * as types from '../constants/ActionTypes'

export const selectItem = id => ({ type: types.SELECT_ITEM, id })
export const showDetails = id => ({ type: types.SHOW_DETAILS, id })
export const itemsFetchDataSuccess = items => ({ type: types.ITEMS_FETCH_DATA_SUCCESS, items })