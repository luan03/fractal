import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import './Card.scss';


class Card extends Component {

  render() {

        let beer = this.props.selected
        
        return <section className="card">
                    <img src={beer.image_url} alt={beer.name} title={beer.name} width="150" height="180" />
                    <br />
                    <h2 className="name">{beer.name}</h2>
                    <br />
                    <span className="tagline">{beer.tagline}</span>
                    <br />
                    <span className="description">{beer.description}</span>
               </section>
    
    }
}

Card.propTypes = {
    image_url: PropTypes.string,
    name: PropTypes.string,
    tagline: PropTypes.string,
    description: PropTypes.string
};

export default connect()(Card);

module.hot.accept();