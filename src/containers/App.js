import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as Actions from '../actions'
import List from './List'

const title = 'Fractal Project';

const App = ({beers, actions}) => (
  <div>
    <div>{title}</div>

    <List />

  </div>
)

export default connect(
  
)(App)